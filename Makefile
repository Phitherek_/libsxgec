CC=gcc
CFLAGS=-Wall
PREFIX=/usr/local

all:
	mkdir -p lib
	mkdir -p include/sxgec
	${CC} ${CFLAGS} -fPIC -c src/sxgec.c -o lib/sxgec.o
	${CC} -shared -Wl,-soname,libsxgec.so.1 -o lib/libsxgec.so.1.0 lib/sxgec.o -lxcb -lxcb-randr -lm
	ln -sf libsxgec.so.1.0 lib/libsxgec.so.1
	ln -sf libsxgec.so.1.0 lib/libsxgec.so
	cp src/sxgec.h include/sxgec
install:
	install -d $(DESTDIR)$(PREFIX)/lib
	install -d $(DESTDIR)$(PREFIX)/include/sxgec
	install -m 644 include/sxgec/* $(DESTDIR)$(PREFIX)/include/sxgec
	install -m 644 lib/libsxgec.so.1.0 $(DESTDIR)$(PREFIX)/lib
	ln -sf $(DESTDIR)$(PREFIX)/lib/libsxgec.so.1.0 $(DESTDIR)$(PREFIX)/lib/libsxgec.so.1
	ln -sf $(DESTDIR)$(PREFIX)/lib/libsxgec.so.1.0 $(DESTDIR)$(PREFIX)/lib/libsxgec.so
test:
	mkdir -p bin
	${CC} ${CFLAGS} src/test.c -o bin/test -lsxgec
clean:
	rm -rf lib
	rm -rf bin
	rm -rf include
