#include <stdio.h>
#include <stdlib.h>
#include <sxgec/sxgec.h>

void callback(char* event_name, void* data) {
    puts(event_name);
}

int main(void) {
    sxgec_catch_events(1, 1, 1, 1, 1, 1, 1, 1, callback, NULL);
    return 0;
}
