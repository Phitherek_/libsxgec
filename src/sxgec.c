#include "sxgec.h"

xcb_connection_t *sxgec_connection;
xcb_screen_iterator_t sxgec_iter;
xcb_screen_t *sxgec_screen;
xcb_window_t sxgec_window;
uint16_t sxgec_values;
xcb_void_cookie_t sxgec_cookie;
xcb_generic_error_t *sxgec_error;
xcb_generic_event_t *sxgec_event;

int sxgec_init(int randr_screen_change, int randr_crtc_change, int randr_output_change, int randr_output_property, int randr_provider_change, int randr_provider_property, int randr_resource_change, int randr_lease) {
    sxgec_connection = xcb_connect(NULL, NULL);
    if(xcb_connection_has_error(sxgec_connection) != 0) {
        puts("XCB connection error!");
        return 1;
    }
    const xcb_setup_t *sxgec_setup = xcb_get_setup(sxgec_connection);
    sxgec_iter = xcb_setup_roots_iterator(sxgec_setup);
    sxgec_screen = sxgec_iter.data;
    sxgec_window = xcb_generate_id(sxgec_connection);

    sxgec_values = 0;

    if(randr_screen_change == 1) {
        sxgec_values |= XCB_RANDR_NOTIFY_MASK_SCREEN_CHANGE;
    }
    if(XCB_RANDR_MINOR_VERSION >= 2) {
        if(randr_crtc_change == 1) {
            sxgec_values |= XCB_RANDR_NOTIFY_MASK_CRTC_CHANGE;
        }
        if(randr_output_change == 1) {
            sxgec_values |= XCB_RANDR_NOTIFY_MASK_OUTPUT_CHANGE;
        }
        if(randr_output_property == 1) {
            sxgec_values |= XCB_RANDR_NOTIFY_MASK_OUTPUT_PROPERTY;
        }
    }
    if(XCB_RANDR_MINOR_VERSION >= 4) {
        if(randr_provider_change == 1) {
            sxgec_values |= XCB_RANDR_NOTIFY_MASK_PROVIDER_CHANGE;
        }
        if(randr_provider_property == 1) {
            sxgec_values |= XCB_RANDR_NOTIFY_MASK_PROVIDER_PROPERTY;
        }
        if(randr_resource_change == 1) {
            sxgec_values |= XCB_RANDR_NOTIFY_MASK_RESOURCE_CHANGE;
        }
    }
    if(XCB_RANDR_MINOR_VERSION >= 6) {
        if(randr_lease == 1) {
            sxgec_values |= XCB_RANDR_NOTIFY_MASK_LEASE;
        }
    }

    sxgec_cookie = xcb_create_window_checked(sxgec_connection, XCB_COPY_FROM_PARENT, sxgec_window, sxgec_screen->root, 0, 0, 1, 1, 1, XCB_WINDOW_CLASS_INPUT_OUTPUT, sxgec_screen->root_visual, 0, NULL);

    sxgec_error = xcb_request_check(sxgec_connection, sxgec_cookie);

    if(sxgec_error != NULL) {
        puts("create window error!");
        return 1;
    }

    sxgec_cookie = xcb_randr_select_input_checked(sxgec_connection, sxgec_window, sxgec_values);

    sxgec_error = xcb_request_check(sxgec_connection, sxgec_cookie);

    if(sxgec_error != NULL) {
        puts("randr select input error!");
        return 1;
    }
    return 0;
}

int sxgec_catch_next_event(void (*callback)(char*, void*), void *data) {
    sxgec_event = xcb_wait_for_event(sxgec_connection);
    if(sxgec_event == NULL) {
        callback("event_io_error", data);
        return 1;
    }
    switch(sxgec_event->response_type) {
        // Experimental values
        case 89: {
            callback("screen_change", data);
            break;
        }
        case 90: {
            callback("output_change", data);
            break;
        }
        default: {
            // I could not decipher the other codes from anywhere
            callback("not_supported", data);
            printf("%u\n", sxgec_event->response_type);
            break;
        }
    }
    free(sxgec_event);
    return 0;
}

void sxgec_catch_events(int randr_screen_change, int randr_crtc_change, int randr_output_change, int randr_output_property, int randr_provider_change, int randr_provider_property, int randr_resource_change, int randr_lease, void (*callback)(char*, void*), void* data) {
    if(sxgec_init(randr_screen_change, randr_crtc_change, randr_output_change, randr_output_property, randr_provider_change, randr_provider_property, randr_resource_change, randr_lease) == 0) {
        while(sxgec_catch_next_event(callback, data) == 0);
        sxgec_free();
    }
}

void sxgec_free() {
    xcb_destroy_window(sxgec_connection, sxgec_window);
    xcb_disconnect(sxgec_connection);
}
