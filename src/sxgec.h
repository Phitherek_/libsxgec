#ifndef _SXGEC_H
#define _SXGEC_H
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <xcb/xcb.h>
#include <xcb/randr.h>

int sxgec_init(int randr_screen_change, int randr_crtc_change, int randr_output_change, int randr_output_property, int randr_provider_change, int randr_provider_property, int randr_resource_change, int randr_lease);

int sxgec_catch_next_event(void (*callback)(char*, void*), void *data);

void sxgec_catch_events(int randr_screen_change, int randr_crtc_change, int randr_output_change, int randr_output_property, int randr_provider_change, int randr_provider_property, int randr_resource_change, int randr_lease, void (*callback)(char*, void*), void *data);

void sxgec_free();

#endif
