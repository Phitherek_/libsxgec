# libsxgec - Simple XRandR Global Event Catcher library

## Why?

I needed to be able to catch XRandR screen/output change events for my auto-display-manager project.

## How?

By magic of XCB and experimentation I managed to isolate the codes for screen change and output change events. The library is prepared to handle all XRandR events, however I was not able to find the rest of the event codes. Help is welcome.

## Prerequisites

* xcb
* xcb-randr
* A C compiler (preferrably GCC)
* GNU make

## Compilation

`make`

## Installation

`sudo make install PREFIX=/installation/prefix/path`

## Usage

There is no documentation for the library, so I will help you by writing it here. It consists of four pretty simple functions:

* `int sxgec_init(int randr_screen_change, int randr_crtc_change, int randr_output_change, int randr_output_property, int randr_provider_change, int randr_provider_property, int randr_resource_change, int randr_lease)` - this function initializes an SXGEC catching window (it is invisible, but it's there). The corresponding parameters, if set to 1 and if XRandR version matches, enable catching the corresponding XRandR events. In practice, only screen change and output change are supported for now, for reasons described above. It returns 0 on success and 1 on any error (errors are written to stdout).

* `int sxgec_catch_next_event(void (*callback)(char*, void*), void *data)` - this function catches the next XRandR event that arrives to the window. The parameter is a callback function that has a C string as a first parameter and custom data as a second parameter. When event is caught, a C String is being passed to the callback function that corresponds to the event name (for now, `screen_change` or `output_change`). In case of error the string is `event_io_error`. It returns 0 on success and 1 on error.

* `void sxgec_free()` - this function destroys the SXGEC catching window and closes the connection.

* `void sxgec_catch_events(int randr_screen_change, int randr_crtc_change, int randr_output_change, int randr_output_property, int randr_provider_change, int randr_provider_property, int randr_resource_change, int randr_lease, void (*callback)(char*, void*), void *data)` - this function performs an endless loop of catching events and calling callback function. It allows for setting up the process with one function, however I do not recommend it because you cannot break out of the inner loop and it does not ensure freeing the resources.

## Test program compilation

`make test CFLAGS=(here -L -I and other options if necessary)`

## Test program execution

`LD_LIBRARY_PATH=(path to lib if needed) bin/test`

The test program implements a callback function that simply displays the returned status codes on the stdout.

## Contributing

By all means! I would certainly use some help, especially with the XRandR event codes.
